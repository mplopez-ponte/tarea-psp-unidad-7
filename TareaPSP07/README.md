## Tarea para PSP07.

### Ejercicio 1.

De igual manera a lo visto en el tema, ahora te proponemos un ejercicio que genere una cadena de texto y la deje almacenada en un fichero encriptado, en la raíz del proyecto hayas creado, con el nombre fichero.cifrado.

Para encriptar el fichero, utilizarás el algoritmo Rijndael o AES, con las especificaciones de modo y relleno siguientes: Rijndael/ECB/PKCS5Padding.

La clave, la debes generar de la siguiente forma:

 * A partir de un número aleatorio con semilla la cadena del nombre de usuario + password.
 * Con una longitud o tamaño 128 bits.

Para probar el funcionamiento, el mismo programa debe acceder al fichero encriptado para desencriptarlo e imprimir su contenido.

Una vez ejecutamos nuestro programa, lo primero que hace es preguntar nuestro usuario tal como vemos en la siguiente imagen:

![Preguntando el usuario](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-7/downloads/Preguntando%20usuario.png)

En el siguiente paso se nos solicitará la contraseña:

![Se solicita la contraseña](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-7/downloads/Preguntando%20contrase%C3%B1a.png)

El siguiente paso es ya que el propio programa genera una cadena de caracterees de 100 letras, y también se va a generar la clave.
Además de generar la cadena de caracteres, también se generará un buffer sin cifrado y a continuación se va a cifrar y finaliza l cifrado.
Una vez finalizado el cifrado, vamos a proceder al descifrado, se almacena el contenido del buffer ya sin cifrar y a continuación desencriptamos la cadena.

![Mostramos el proceso completo](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-7/downloads/Proceso%20completo.png)

En este punto, nuestro programa podrá encriptar y desencriptar los datos sin problema, y en este caso, el programa generará un archivo que estará cifrado y que quedará
almacenado en la carpeta de nuestro proyecto, tal como se muestra a continuación:

![Mostrando el archivo cifrado](https://bitbucket.org/mplopez-ponte/tarea-psp-unidad-7/downloads/Estructura%20del%20proyecto.png)