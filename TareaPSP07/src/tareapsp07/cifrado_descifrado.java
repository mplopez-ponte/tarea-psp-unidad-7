package tareapsp07;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Scanner;
import javax.crypto.*;

public class cifrado_descifrado {
	public static void main(String[] args)  throws Exception {
	
		//Vamos a generar una cadena de texto de 100 letras para poder encriptar
		String string_cad ="",user,passwd;
		Random int_random = new Random();
		for (int i = 0; i < 100; i++) {
			string_cad += (char)(int_random.nextInt(25) + 97); //Letras minúsculas
		}
	
		//Vamos a solicitar el usuario y la contraseña a través de teclado
		Scanner teclado = new Scanner(System.in);
		System.out.println("Usuario: ");
		user = teclado.nextLine();
		System.out.println("Contraseña: ");
		passwd = teclado.nextLine();
		teclado.close();
		//Se muestra la cadena generada por pantalla
		System.out.println("LA CADENA YA GENERADA");
		System.out.println(string_cad);
		//Vamos a generar la clave 
		KeyGenerator genKey = KeyGenerator.getInstance("AES"); //Usando algoritmo AES
		SecureRandom randomNumero = SecureRandom.getInstance("SHA1PRNG");
		//Creamos la semilla con el usuario y la contraseña
		randomNumero.setSeed((user+passwd).getBytes());
		genKey.init(128,randomNumero); // 128 bits con número aleatorio
		SecretKey clave = genKey.generateKey(); //Clave
		//Se muestra a través de consola la clave
		System.out.println("LA CLAVE");
		System.out.println(clave);
		//Usamos el cifrador Cipher
		Cipher cifrador_cipher = Cipher.getInstance("Rijndael/ECB/PKCS5Padding");
		cifrador_cipher.init(Cipher.ENCRYPT_MODE, clave);
		//Los buffers en Claro y Cifrado
		byte[] bf_Claro = string_cad.getBytes();
		byte[] bf_Cifrado = cifrador_cipher.doFinal(bf_Claro);
		//Mostramos los buffers sin cifrar y cifrado
		System.out.println("BUFFER SIN CIFRAR");
		System.out.println(bf_Claro);
		System.out.println("BUFFER YA CIFRADO");
		System.out.println(bf_Cifrado);
		//Almacenamos en un fichero el buffer ya cifrado
		FileOutputStream archCifrado = new FileOutputStream("fichero.cifrado");
		archCifrado.write(bf_Cifrado);
		archCifrado.close();
		System.out.println("FIN DEL CIFRADO\nCOMENZAMOS EL DESCIFRADO");
		//Vamos a descifrar el archivo
		cifrador_cipher.init(Cipher.DECRYPT_MODE, clave); 
		
		//Se lee el buffer ya cifrado del archivo y se va a descifrar
		bf_Cifrado = Files.readAllBytes(Paths.get("fichero.cifrado"));
		bf_Claro = cifrador_cipher.doFinal(bf_Cifrado);
		//Se muestra a través de consola los buffers ya leídos
		System.out.println("BUFFER SIN CIFRAR");
		System.out.println(bf_Claro);
		System.out.println("BUFFER YA CIFRADO");
		System.out.println(bf_Cifrado);
		
		//Sacamos por consola la cadena leida
		String strRead = new String(bf_Claro);
		System.out.println("LA CADENA YA ESTÁ DESENCRIPTADA");
		System.out.println(strRead);
	}

}